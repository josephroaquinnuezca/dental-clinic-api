module.exports = {
    USER: '/v1/public/users',
    USER_PRIVATE: '/v1/private/users',
    GET_ERROR: '/v1/errors'
  }