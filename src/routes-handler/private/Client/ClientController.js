const config = require("config").get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;

//LOCAL
const ClientService = require("../../../services/ClientService");

const reqResponse = require("../../../cors/responseHandler");
const { validationResult } = require("express-validator");
const chalk = require("chalk");
const async = require("async");


module.exports = {


  getAllClient: async (req, res) => {
    try {

      let page = parseInt(req.query.page);
			let limit = parseInt(req.query.size);

			let searchString = req.query.searchString;
	  
			const offset = page ? page * limit : 0;


			let result = await ClientService.getAllClient(limit, offset, searchString);
	  
			
			const totalPages = Math.ceil(result.count / limit);
	  
			const result_pagination = {
			  totalCount: result.count,
			  totalPages: totalPages,
			  pageNumber: page,
			  pageSize: result.rows.length,
			  users: result.rows,
			};

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully gathered user master data.",
            result_pagination
          )
        );
    } catch (error) {
      // console.error(error)
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  getSingleClient: async (req, res) => {
    try {
      const id = req.params.id;
      let result = await ClientService.getSingleClient(id);
      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully gathered user master data.",
            result
          )
        );
    } catch (error) {
      // console.error(error)
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  CreateALLClient: async (req, res) => {
    try {
      let user_result = await ClientService.CreateALLClient(req.body);

      let dataValues = user_result.dataValues;
      console.log(dataValues);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Create Team Member data.",
            user_result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },


  //weekly report
  gellAllperWeekUser: async (req, res) => {
    try {

      //clientservice
      let startdate = req.query.startdate;
      let enddate = req.query.enddate;

      let user_result = await ClientService.gellAllperWeekUser(startdate, enddate);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Created a report.",
            user_result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //monthly user

  gellAllperMonthUser: async (req, res) => {
    try {

      //clientservice
      let date = req.query.date;
      // let enddate = req.query.enddate;

      let user_result = await ClientService.gellAllperMonthUser(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Created a report.",
            user_result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //yearly
  gellAllperYearUser: async (req, res) => {
    try {

      //clientservice
      let date = req.query.date;
      // let enddate = req.query.enddate;

      let user_result = await ClientService.gellAllperYearUser(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Created a report.",
            user_result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //
  UpdateALLClient: async (req, res) => {
    try {
      const id = req.params.id;

      //end
      let user_result = await ClientService.UpdateALLClient(req.body, id);

      // let dataValues = user_result.dataValues
      // console.log(dataValues)

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Updated the data.",
            user_result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  GetCountAll: async (req, res) => {
    try {
      //end
      let result = await ClientService.GetCountAll();

      console.log("result", result)

      // for(res of result){
        
      // }
      

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Updated the data.",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  ConfirmedClient: async (req, res) => {
    try {
      let id = req.query.id;

      let result = await ClientService.ConfirmedClient(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Updated the data.",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  GetCountWithDate: async (req, res) => {
    try {
      
      // QUERY PARAMS

      let date = req.query.date

      let result = await ClientService.GetCountWithDate(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Updated the data.",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  getalllUsersBydate: async (req, res) => {
    try {
      
      // QUERY PARAMS

      let date = req.query.date

      let result = await ClientService.getalllUsersBydate(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Updated the data.",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  DeleteClient: async (req, res) => {
    try {
      
      // QUERY PARAMS

      let id = parseInt(req.query.id)

      let result = await ClientService.DeleteClient(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Deleted data.",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  RejectClient: async (req, res) => {
    try {
      
      // QUERY PARAMS

      let id = parseInt(req.query.id)

      let result = await ClientService.RejectClient(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Reject the data.",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },
  
  
};
