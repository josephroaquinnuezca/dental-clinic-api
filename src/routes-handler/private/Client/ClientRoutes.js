const router = require('express').Router();

//GET TEAM USERS DATA
const ClientController = require('./ClientController');
const Validation = require('../../../validation/validation')


const RouteConstant = require('../../../constant/Routes');

module.exports = (app) => {



    router.route('/get-all-client')
    .get(
        ClientController.getAllClient
    );

    router.route('/get-single-client/:id')
    .get(
        ClientController.getSingleClient
    );

    router.route('/create-client')
    .post(
        ClientController.CreateALLClient
    );

    router.route('/update-client/:id')
    .patch(
        ClientController.UpdateALLClient
    );

    // count
    router.route('/get-all-count')
    .get(
        ClientController.GetCountAll
    );

    //updated confirmed
    router.route('/confirmed-updated-appointment')
    .get(
        ClientController.ConfirmedClient
    );
    
    //with date
    router.route('/get-all-count-by-date')
    .get(
        ClientController.GetCountWithDate
    );

    router.route('/get-all-users-by-date')
    .get(
        ClientController.getalllUsersBydate
    );

    router.route('/delete-client-information')
    .get(
        ClientController.DeleteClient
    );
    
    router.route('/reject-client-information')
    .get(
        ClientController.RejectClient
    );
    


  


  

    //weekly report date
    router.route('/get-all-per-week-users')
    .get(
        ClientController.gellAllperWeekUser
    );

    router.route('/get-all-per-monthly-users')
    .get(
        ClientController.gellAllperMonthUser
    );

    
    router.route('/get-all-per-yearly-users')
    .get(
        ClientController.gellAllperYearUser
    );


    

    

app.use(
    RouteConstant.USER_PRIVATE,
    // Middleware,
    router
);
};



