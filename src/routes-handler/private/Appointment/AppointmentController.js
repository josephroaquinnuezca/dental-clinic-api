const config = require("config").get(mode);
const keysConfig = config.keys;

//LOCAL
const AppointmentService = require("../../../services/AppointmentService");

const reqResponse = require("../../../cors/responseHandler");
const { validationResult } = require("express-validator");
const chalk = require("chalk");
const async = require("async");

module.exports = {
  UpdateAlldateAppointment: async (req, res) => {
    try {
      // const date = req.params.date;
      let date = req.query.date;

      console.log("date", date);

      let result = await AppointmentService.UpdateAlldateAppointment(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Single data.",
            result
          )
        );
    } catch (error) {
      // console.error(error)
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  EditSingleAppointment: async (req, res) => {
    try {
      const id = req.params.id;

      let result = await AppointmentService.EditSingleAppointment(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Single data.",
            result
          )
        );
    } catch (error) {
      // console.error(error)
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  getSingleAppointment: async (req, res) => {
    try {
      const data = req.body.datechangeValue;

      console.log("date ngayun", data);

      let result = await AppointmentService.getSingleAppointment(data);
      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully gathered user master data.",
            result
          )
        );
    } catch (error) {
      // console.error(error)
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //NOTES --
  //PASSWORD MUST BE HASHE BEFORE SAVING INTO THE DATABASE -- NOT YET DONE

  getAllAppointment: async (req, res) => {
    try {

      let page = parseInt(req.query.page);
			let limit = parseInt(req.query.size);

			let searchString = req.query.searchString;

      const offset = page ? page * limit : 0;

      let result = await AppointmentService.getAllAppointment(limit, offset, searchString);

      console.log("Result", result)

      
			const totalPages = Math.ceil(result.count / limit);
	  
			const result_pagination = {
			  totalCount: result.count,
			  totalPages: totalPages,
			  pageNumber: page,
			  pageSize: result.rows.length,
			  data: result.rows,
			};


      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully gathered data.",
            result_pagination
          )
        );
    } catch (error) {
      // console.error(error)
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  CreateAppointment: async (req, res) => {
    
    try {

      // const unique_id = Math.random().toString(26).slice(2);

      // const todaysDate = new Date()
      // const currentYear = todaysDate.getFullYear()

      // console.log(unique_id)


      const req_body = {
        // client_id: req.body.client_id,
        appointment_date: req.body.appointment_date,
        appointment_time: req.body.appointment_time,
        appointment_category: req.body.appointment_category,
      };

      console.log("hello", req_body);

      //end
      let user_result = await AppointmentService.CreateAppointment(req_body);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Create Team Member data.",
            user_result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  CreateClientAppointment: async (req, res) => {
    try {

      const req_body = {
        client_id: req.body.client_id,
        appointment_date: req.body.appointment_date,
        appointment_time: req.body.appointment_time,
        appointment_category: req.body.appointment_category,
        month: req.body.month,
      };

      console.log("hello", req.body);

      //end
      let result = await AppointmentService.CreateClientAppointment(req_body);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Created Appointment",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  GetClientAppointment: async (req, res) => {
    try {
      const id = req.params.id;

      console.log("ID", id);

      let date = req.query.date;

      console.log("date", date)

      //end
      let result = await AppointmentService.GetClientAppointment(id, date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Appointment",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //new modified
  GetAllClientAppointment: async (req, res) => {
    try {

      let id = req.params.id;
      let page = parseInt(req.query.page);
			let limit = parseInt(req.query.size);

			let searchString = req.query.searchString;
	  
			const offset = page ? page * limit : 0;
			let result = await AppointmentService.GetAllClientAppointment(limit, offset, searchString, id);
	  
			
			const totalPages = Math.ceil(result.count / limit);
	  
			const result_pagination = {
			  totalCount: result.count,
			  totalPages: totalPages,
			  pageNumber: page,
			  pageSize: result.rows.length,
			  users: result.rows,
			};


      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Appointment",
            result_pagination
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  DeleteSingleAppointment: async (req, res) => {
    try {
      const id = req.params.id;

      console.log("ID", id);

      //end
      let result = await AppointmentService.DeleteSingleAppointment(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Delete Appointment",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //new modifiying
  GetAllClientAppointmentAdmin: async (req, res) => {
    try {

 
      let page = parseInt(req.query.page);
			let limit = parseInt(req.query.size);

			let searchString = req.query.searchString;
	  
			const offset = page ? page * limit : 0;

      let result = await AppointmentService.GetAllClientAppointmentAdmin(limit, offset, searchString);

      const totalPages = Math.ceil(result.count / limit);

      const result_pagination = {
			  totalCount: result.count,
			  totalPages: totalPages,
			  pageNumber: page,
			  pageSize: result.rows.length,
			  users: result.rows,
			};

      
      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Appointment",
            result_pagination
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  GetAllClientAppointmentToday: async (req, res) => {
    try {
      let date = req.query.date;

      let result = await AppointmentService.GetAllClientAppointmentToday(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Appointment",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //MANAGE APPOINTMENT
  UpdateSingleWaitingAppointment: async (req, res) => {
    try {
      
      let id = parseInt(req.query.id);
      let clientid = parseInt(req.query.clientid);

      let result = await AppointmentService.UpdateSingleWaitingAppointment(id, clientid);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Get Appointment",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  updateAppointmentAdmin: async (req, res) => {
    try {

     const id = req.params.id;

      let result = await AppointmentService.updateAppointmentAdmin(req.body, id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Updated Appointment",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  rejectAppointmentClient: async (req, res) => {
    try {

     const id = parseInt(req.query.id)

     const contactno = req.query.contactno



      let result = await AppointmentService.rejectAppointmentClient(id, contactno);

      res
        .status(201)
        .send(
          reqResponse.successResponse(
            201,
            "Successfully Updated Appointment",
            result
          )
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  

  

  //APPOINTMENT HISTORY

  updateUserAppointment: async (req, res) => {
    try {

      const id = req.params.id;

      console.log("FROM CONTROLLER", req.body)

      //end
      let result = await AppointmentService.updateUserAppointment(req.body, id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Create  data.", result)
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },


  
  getSingleaAppointmentUser: async (req, res) => {
    try {

      const id = req.params.id;

      console.log("id from contorller", id);

      //end
      let result = await AppointmentService.getSingleaAppointmentUser(id);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Create  data.", result)
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },


  //calendar
  fetchAllClientAppointmentCalendar: async (req, res) => {
    try {


      //end
      let result = await AppointmentService.fetchAllClientAppointmentCalendar();

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Create  data.", result)
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  GetAllReportUsersList: async (req, res) => {
    try {

      let date = req.query.date;

      //end
      let result = await AppointmentService.GetAllReportUsersList(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Create  data.", result)
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  GetAllReportMonthlyList: async (req, res) => {
    try {

      let date = req.query.date;

      //end
      let result = await AppointmentService.GetAllReportMonthlyList(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Create  data.", result)
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  GetAllReportYearlyList: async (req, res) => {
    try {

      let date = req.query.date;

      //end
      let result = await AppointmentService.GetAllReportYearlyList(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Create  data.", result)
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  //NOT FINISH
  GetAllCountReportYear: async (req, res) => {
    try {

      let date = req.query.date;

      //end
      let result = await AppointmentService.GetAllCountReportYear(date);

      res
        .status(201)
        .send(
          reqResponse.successResponse(201, "Successfully Create  data.", result)
        );
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },







  // SendMessage: async (req, res) => {
  // 	try {

  // 		const itexmo = require('itexmo')({ apiCode: 'TR-DENTA678410_XALJT' })

  // 		itexmo
  // 		.send({ to: '09475633709', body: 'hello world' })
  // 		.then(message => console.log(message))
  // 	} catch (error) {
  // 		// console.error(error)
  // 		res.status(502).send(reqResponse.errorResponse(502, String(error + "message sms" )))
  // 	}
  // },
};
