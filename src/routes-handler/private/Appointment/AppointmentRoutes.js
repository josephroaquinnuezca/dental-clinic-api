const router = require('express').Router();

//GET TEAM USERS DATA
const AppointmentController = require('./AppointmentController');


const RouteConstant = require('../../../constant/Routes');

module.exports = (app) => {

    router.route('/update-all-appointment/')
    .get(
        AppointmentController.UpdateAlldateAppointment
    );

    router.route('/edit-single-appointment/:id')
    .get(
        AppointmentController.EditSingleAppointment
    );

  

    router.route('/get-all-appointment')
    .get(
        AppointmentController.getAllAppointment
    );

    router.route('/create-appointment')
    .post(
        AppointmentController.CreateAppointment
    );

    router.route('/get-single-appointment')
    .post(
        AppointmentController.getSingleAppointment
    );

    router.route('/create-client-appointment')
    .post(
        AppointmentController.CreateClientAppointment
    );

    router.route('/get-client-appointment/:id')
    .get(
        AppointmentController.GetClientAppointment
    );

    //new modified
    router.route('/get-all-client-appointment/:id')
    .get(
        AppointmentController.GetAllClientAppointment
    );

    router.route('/delete-single-appointment/:id')
    .delete(
        AppointmentController.DeleteSingleAppointment
    );
    
    //NEW ADMIN
    router.route('/get-all-client-appointment-admin')
    .get(
        AppointmentController.GetAllClientAppointmentAdmin
    );

    router.route('/get-all-client-appointment-today')
    .get(
        AppointmentController.GetAllClientAppointmentToday
    );

    // MANAGE APPOINTMENT
    
    router.route('/update-single-waiting-appointment')
    .get(
        AppointmentController.UpdateSingleWaitingAppointment
    );

    router.route('/update-appointment-admin/:id')
    .post(
        AppointmentController.updateAppointmentAdmin
    );

    router.route('/reject-client-appointment')
    .get(
        AppointmentController.rejectAppointmentClient
    );

    


    
    //APPOINTMENT HISTORY
    router.route('/update-single-user-appointment/:id')
    .patch(
        AppointmentController.updateUserAppointment
    );

    router.route('/get-single-appointment-user/:id')
    .get(
        AppointmentController.getSingleaAppointmentUser
    );

    //calendar
    router.route('/fetch-all-client-appointment-calendar')
    .get(
        AppointmentController.fetchAllClientAppointmentCalendar
    );

    //report

    router.route('/get-all-report-users-list')
    .get(
        AppointmentController.GetAllReportUsersList
    );
    
    //month
    router.route('/get-all-per-monthly-appointment')
    .get(
        AppointmentController.GetAllReportMonthlyList
    );
    
    //year
    router.route('/get-all-per-year-appointment')
    .get(
        AppointmentController.GetAllReportYearlyList
    );

    //
      //year
      router.route('/get-all-per-year-count-report')
      .get(
          AppointmentController.GetAllCountReportYear
      );

  

app.use(
    RouteConstant.USER_PRIVATE,
    // Middleware,
    router
);
};


