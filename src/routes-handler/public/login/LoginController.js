const config = require("config").get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;

//LOCAL
const ClientService = require("../../../services/ClientService");

const reqResponse = require("../../../cors/responseHandler");
const { validationResult } = require("express-validator");
const chalk = require("chalk");
const async = require("async");

let jwt = require("jsonwebtoken");

module.exports = {
  //NOTES --
  //PASSWORD MUST BE HASHE BEFORE SAVING INTO THE DATABASE -- NOT YET DONE

  logIn: async (req, res) => {
    try {
      const valErrors = validationResult(req);
      if (!valErrors.isEmpty()) {
        console.log(valErrors);
        if (valErrors.errors[0].value == undefined) {
          return res
            .status(402)
            .send(reqResponse.errorResponse(402, valErrors));
        } else {
          return res
            .status(403)
            .send(reqResponse.errorResponse(403, valErrors));
        }
      }

      const { username, password } = req.body;
      console.log(username);
      console.log(password);

      //end
      let result = await ClientService.login(username, password);

      let userData = result.dataValues;
      delete userData.password;
      console.log("USERS DATA", userData);

      // create a token
      var token = jwt.sign({ auth_data: userData }, corsConfig.secret, {
        expiresIn: 86400, // expires in 24 hours
      });

      console.log("token", token);

      auth_data = {
        token: token
      }

      console.log("auth data", auth_data);

      // Set the options for the cookie
      let cookieOptions = {
        // Delete the cookie after 90 days
        expires: new Date(Date.now() + 1 * 24 * 60 * 60 * 1000),
        // Set the cookie's HttpOnly flag to ensure the cookie is
        // not accessible through JS, making it immune to XSS attacks
        httpOnly: true
      }

      if (
        process.env.NODE_ENV === "production" ||
        process.env.NODE_ENV === "staging"
      ) {
        cookieOptions.secure = true;
      } else {
        cookieOptions.secure = false;
      }

      res.cookie('jwt', token, cookieOptions).status(201).send(reqResponse.successResponse( 201,"Successfully gathered user data.", auth_data));
      
    } catch (error) {
      console.error(error);
      res.status(502).send(reqResponse.errorResponse(502, String(error)));
    }
  },

  logout: async (req, res) => {
		try {
			res.clearCookie('jwt').status(201).send(reqResponse.successResponse(201, "Successfully gathered user data for logout.", "Successfull logout user."))
		} catch (error) {
			console.error('catchFromController authLogoutUser: ', error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)))
		}
	},

  
  
};
