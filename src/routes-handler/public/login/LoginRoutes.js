const router = require('express').Router();

//GET TEAM USERS DATA
const loginController = require('./LoginController');
const Validation = require('../../../validation/validation')


const RouteConstant = require('../../../constant/Routes');

module.exports = (app) => {

    router.route('/log-in')
    .post(
        loginController.logIn
    );

    router.route('/logout')
    .get(
        loginController.logout
    );


    

app.use(
    RouteConstant.USER,
    // Middleware,
    router
);
};



