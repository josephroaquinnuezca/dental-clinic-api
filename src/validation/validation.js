const { body, check } = require('express-validator');

// CONSIST SECTION OF "PRIVATE ROUTES" and "PUBLIC ROUTES"
// SEARCH FOR THE SECTION FOR EASY NAVIGATION.

module.exports = {
  //PRIVATE ROUTES
//   create: () => {
//     return [
//       check("name", "Name is required!").not().isEmpty(),
//     ]
//   },

//   updateUser: () => {
//     return [
//       // check('username', 'username is Mandatory Parameter Missing.').not().isEmpty(),
//       // check('email_address', 'email_address is Mandatory Parameter Missing.').not().isEmpty(),
//       check('name', 'name is Mandatory Parameter Missing.').not().isEmpty(),
//     ]
//   },
  //PUBLIC ROUTES
  authLoginUser: () => {
    return [
      check('username', 'username is Mandatory Parameter Missing.').not().isEmpty(),
      check('password', 'password is Mandatory Parameter Missing.').not().isEmpty(),
    ]
  }
//   createTestUser: () => {
//     return [
//       check('username', 'username is Mandatory Parameter Missing.').not().isEmpty(),
//       check('email_address', 'email_address is Mandatory Parameter Missing.').not().isEmpty(),
//       check('password', 'password is Mandatory Parameter Missing.').not().isEmpty(),
//       check('name', 'name is Mandatory Parameter Missing.').not().isEmpty(),
//     ]
//   }
}
