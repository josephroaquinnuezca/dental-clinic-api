module.exports = (sequelize, Sequelize) => {
    const ClientAppointment = sequelize.define("client_appointment_table", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },

      client_id: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      user_appointment_id_number: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },

     
      // clientTableId: {
      //   defaultValue: null,
      //   allowNull: true,
      //   type: Sequelize.STRING
      // },

      appointment_date: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      appointment_time: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.TEXT
      },

      appointment_category: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.TEXT
      },
      user_status: {
        defaultValue: "Waiting",
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      status: {
        defaultValue: null,
        type: Sequelize.STRING(255)
      },

      month: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.STRING(255)
      },

      
      year: {
        allowNull: true,
        defaultValue: null,
        type: Sequelize.STRING(255)
      },

      created_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    
    }, {
      freezeTableName: true
    });
    return ClientAppointment;
  };