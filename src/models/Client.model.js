module.exports = (sequelize, Sequelize) => {
    const client_table = sequelize.define("client_table", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      dob: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      gender: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },

      username: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      password: {
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      contact_no: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.BIGINT(11)
      },
      email: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      age: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(11)
      },
      address: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      user_role: {
        defaultValue: 0,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      user_status: {
        defaultValue: "Waiting",
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      status: {
        allowNull: true,
        defaultValue: "9",
        type: Sequelize.INTEGER
      },
      date_created: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      ceated_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      }
    
    }, {
      freezeTableName: true
    });
    return client_table;
  };