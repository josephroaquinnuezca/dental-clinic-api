const config = require('config').get(mode);
const dbConfig = config.database;
const Sequelize = require("sequelize");
// constant

const sequelize = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  dialect: dbConfig.dialect,
  operatorsAliases: dbConfig.operatorsAliases,
  logging: dbConfig.logging,
  define: {
    timestamps: dbConfig.timestamps,
    // freezeTableName: dbConfig.freezeTableName
  },
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//joseph nuezca -- 31-07-2021
db.client_table = require("./Client.model")(sequelize, Sequelize);
db.appointment_table = require("./Appointment.model")(sequelize, Sequelize);
db.client_appointment_table = require("./ClientAppointment.model")(sequelize, Sequelize);


// db.client_appointment_table.belongsTo(db.client_table);

db.client_appointment_table.belongsTo(db.client_table, {foreignKey: 'client_id'});  //paki comment  tas uncomment nalang 


module.exports = db;