module.exports = (sequelize, Sequelize) => {
    const AppointmentModel = sequelize.define("appointment_table", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      appointment_date: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      appointment_time: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.TEXT
      },

      appointment_category: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.TEXT
      },
      user_status: {
        defaultValue: "Waiting",
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      status: {
        defaultValue: "Available",
        allowNull: true,
        type: Sequelize.STRING(255)
      },
    
    }, {
      freezeTableName: true
    });
    return AppointmentModel;
  };