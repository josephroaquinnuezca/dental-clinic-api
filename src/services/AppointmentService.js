const { QueryTypes } = require("sequelize");
// declaring models
const db = require("../models");

//initializing models
const AppointmentModel = db.appointment_table;
const ClientAppointmentModel = db.client_appointment_table;
const ClientModel = db.client_table;




// ClientAppointmentModel.hasOne(ClientModel);
// ClientModel.belongsTo(ClientAppointmentModel);



const Op = db.Sequelize.Op;
const chalk = require("chalk");
const async = require("async");

const bcrypt = require("bcrypt");
const itexmo = require("itexmo")({ apiCode: "TR-JOSEP633709_I43AF" });

const Vonage = require("@vonage/server-sdk");

const vonage = new Vonage({
  apiKey: "3bcc2786",
  apiSecret: "TajYgtKypl2vwFYA",
});

module.exports = {
  UpdateAlldateAppointment: async (date) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.red("GET SINGLE APPOINTMENT"));

      var query =
        "UPDATE appointment_table SET status = 'Not Available' WHERE appointment_date < :date";

      await db.sequelize
        .query(query, {
          replacements: { date: date },
          plain: false,
          raw: false,
          type: QueryTypes.UPDATE,
        })
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  EditSingleAppointment: async (id) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.red("GET SINGLE APPOINTMENT"));

      // console.log("date", date);

      AppointmentModel.findOne({
        where: {
          id: id,
        },
      })
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  getSingleAppointment: async (date) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.red("GET SINGLE APPOINTMENT"));

      console.log("date", date);

      AppointmentModel.findOne({
        where: {
          appointment_date: date,
        },
      })

        .then((data) => {
          if (data) {
            console.log(chalk.green("data:", JSON.stringify(data)));
            resolve(data);
          } else {
            reject("Theres no scheduled created in" + " " + date);
          }

          // console.log(chalk.green('data:', data));
          // resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  //CREATE USER FUNCTIONS
  getAllAppointment: async (limit, offset, searchString) => {
    return new Promise(async(resolve, reject) => {
      console.log(chalk.red("FUNCTION:getAllAppointment"));

      const condition = searchString ? {
        [Op.or]: [
          {
            name: {
              [Op.like]: `%${searchString}%`,
            },
          },
        ],
      } : null;

      await AppointmentModel.findAndCountAll({
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          // console.log(chalk.green('data:', data));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  CreateAppointment: async (req_body) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.yellow("TEAM USER Info:", req_body));

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA

      var data_message = {
        // client_id: req_body.client_id,
        appointment_date: req_body.appointment_date,
        appointment_time: req_body.appointment_time,
        appointment_category: req_body.appointment_category,
      };

      // const from = "Vonage APIs"
      // const to = "639952987343"
      // const text = 'Mag pa vaccine kana lodi'

      // vonage.message.sendSms(from, to, text, (err, responseData) => {
      //     if (err) {
      //         console.log(err);
      //     } else {
      //         if(responseData.messages[0]['status'] === "0") {
      //             console.log("Message sent successfully.");
      //         } else {
      //             console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`);
      //         }
      //     }
      // })

      AppointmentModel.create(data_message)
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          // console.log(chalk.green('data:', data));

          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //CLIENT SIDE
  CreateClientAppointment: async (req_body) => {
    return new Promise(async(resolve, reject) => {
      console.log(chalk.yellow("TEAM USER Info:", req_body));

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var dateObj = new Date();
      var month = dateObj.getUTCMonth() + 1; //months from 1-12
      var day = dateObj.getUTCDate();
      var year = dateObj.getUTCFullYear();

      let r = (Math.random() + 1).toString(36).substring(7);
      let x = (Math.random() + 1).toString(36).substring(3);

      const unique_id = year  + r + month +  x + req_body.client_id

     

      var data_message = {
        client_id: req_body.client_id,
        user_appointment_id_number: unique_id,
        appointment_date: req_body.appointment_date,
        appointment_time: req_body.appointment_time,
        appointment_category: req_body.appointment_category,
        month: req_body.month
      };

      const project = await ClientAppointmentModel.findOne({ where: { appointment_time: data_message.appointment_time,  appointment_date: data_message.appointment_date  } });
      
      if (project === null) {
        
        await ClientAppointmentModel.create(data_message)
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);

          // if(data.client_id == data_message.client_id && data.appointment_date == data_message.appointment_date){
          //   reject("You are already Scheduled");
         
          // }else{
          //   resolve(data);
          // }
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    

      
      } else {

        reject("You are already Scheduled")

        // const elseme = await ClientAppointmentModel.findOne({ where: { client_id: data_message.client_id,  appointment_date: data_message.appointment_date  } });
       
        // if(elseme === true){
        //   reject("You are already Scheduled");
        // }


      }
    

      // First, we start a transaction and save it into a variable
      // const t = await db.sequelize.transaction();

      // const finduser = await ClientAppointmentModel.findOne({
      //   firstName: 'Bart',
      //   lastName: 'Simpson'
      // }, { transaction: t });

      // await ClientAppointmentModel.create(data_message, {
      //   transaction: t 
      // })
      //   .then(async(data) => {
      //     console.log(chalk.green("CreateClientAppointment:", JSON.stringify(data)));

      //     console.log("Test", data.id)
          
      //     if(data.id == data_message ){
      //       console.log("Test", data.id)
      //     }else{
      //       resolve(data);
      //       await t.commit();
      //     }

         

          // console.log(data.client_id)

          // if (!data) {
          //   ClientAppointmentModel.create(data_message)
          //     .then((data) => {
          //       console.log(chalk.green("data:", JSON.stringify(data)));
          //       resolve(data);
          //     })
          //     .catch((err) => {
          //       console.log(chalk.red("err: ey", err));
          //       reject(err);
          //     });
          // } else {
          //   if (
          //     data.user_status === "Scheduled" &&
          //     data.client_id == data_message.client_id &&
          //     data.appointment_date == data_message.appointment_date
          //   ) {
          //     reject("You are already Scheduled");
          //   } else {
          //     if (
          //       data.client_id == data_message.client_id &&
          //       data.appointment_date == data_message.appointment_date
          //     ) {
          //       ClientAppointmentModel.update(data_message, {
          //         where: {
          //           client_id: data_message.client_id,
          //           appointment_date: data_message.appointment_date,
          //         },
          //       })
          //         .then((data) => {
          //           console.log(chalk.green("data:", JSON.stringify(data)));
          //           resolve(data);
          //         })
          //         .catch((err) => {
          //           console.log(chalk.red("err: ey", err));
          //           reject(err);
          //         });
          //     } else {
          //       ClientAppointmentModel.create(data_message)
          //         .then((data) => {
          //           console.log(chalk.green("data:", JSON.stringify(data)));
          //           resolve(data);
          //         })
          //         .catch((err) => {
          //           console.log(chalk.red("err: ey", err));
          //           reject(err);
          //         });
          //     }
          //   }
          // }




    //     })
    //     .catch(async(err) => {
    //       console.log(chalk.red("err: ey", err));
    //       reject(err);
    //         // If the execution reaches this line, an error was thrown.
    //         // We rollback the transaction.
    //         await t.rollback();
    //     });
    });
  },

  GetClientAppointment: async (id, date) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.yellow("USER ID:", id));

      var datetostring = toString(date)

      ClientAppointmentModel.findAll({
        where: {
         
          client_id: id,
          appointment_date: date
         

          // [Op.and]: [{
          //   appointment_date: {
          //     [Op.and]: date,
          //   },
          //   client_id: {
          //     [Op.and]: id,
          //   }
           
          // }],

        
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //new modify
  GetAllClientAppointment: async (limit, offset, searchString, id) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.yellow("USER ID:", id));

      const condition = {
        [Op.or]: [
          {
            appointment_category: {
              [Op.like]: `%${searchString}%`,
            },
          },
        ],
        [Op.and]: [
          {
            client_id: id,
          },
        ],
      };

      ClientAppointmentModel.findAndCountAll({
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  DeleteSingleAppointment: async (id) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("USER ID:", id));

      await AppointmentModel.destroy({
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //NEW modifyng
  GetAllClientAppointmentAdmin: async (limit, offset, searchString) => {
    return new Promise((resolve, reject) => {
      // var query =
      //   "SELECT client_appointment_table.id, client_appointment_table.client_id, client_appointment_table.appointment_date, client_appointment_table.appointment_time, client_appointment_table.appointment_category, client_appointment_table.user_status, client_table.name FROM client_appointment_table INNER JOIN client_table ON client_appointment_table.client_id = client_table.id;";

      // db.sequelize
      //   .query(query, {
      //     plain: false,
      //     raw: false,
      //     type: QueryTypes.SELECT,
      //   })
        const condition = {
          [Op.or]: [
            {
              name: {
                [Op.like]: `%${searchString}%`,
              },
            },
          ],
         
        };

        ClientAppointmentModel.findAndCountAll({
          limit: limit,
          offset: offset,
          include: [
            { model: ClientModel, where: condition}
         ],
        })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  GetAllClientAppointmentToday: async (date) => {
    return new Promise((resolve, reject) => {
      var query =
        "SELECT count(*) as count_all, (SELECT COUNT(*) FROM client_appointment_table WHERE appointment_date = :date) AS count_today FROM client_appointment_table;";

      db.sequelize
        .query(query, {
          replacements: { date: date },
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
        })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  // MANAGE APPOINTMENT
  UpdateSingleWaitingAppointment: async (id, clientid) => {
    return new Promise(async (resolve, reject) => {
      console.log("id", clientid);

      const update = {
        user_status: "Scheduled",
      };

      db.sequelize
        .query(
          "SELECT A.id, A.client_id, A.appointment_date, A.user_appointment_id_number, A.appointment_time, A.appointment_category, B.user_status, B.name, B.contact_no FROM client_appointment_table A INNER JOIN client_table B ON A.client_id = B.id WHERE A.id = :clientid",
          {
            replacements: { clientid: id },
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(
            chalk.red("UpdateSingleWaitingAppointment", JSON.stringify(data))
          );

          if (!data) {
          } else {
            data.find((obj) => {
              //please comment here ctrl + / 394
              const from = "Dental Clinic";
              const to = "63" + obj.contact_no;
              const text =
                "Hello," +
                " " +
                obj.name +
                " " +
                "you have an appointment for" +
                " " +
                obj.appointment_category +
                "." +
                "  " +
                "You are scheduled for" +
                " " +
                obj.appointment_date +
                "." +
                "  " +
                "Time:" +
                " " +
                obj.appointment_time +
                "." +
                "  " +
                "  " +
                "Please present this code for confirmation:" +
                " " +
                obj.user_appointment_id_number +
                "." +

                



                "Thank you for your time, Have a good day." +
                "  " +
                "From: Dr. Rizza Dental Clinic";

              vonage.message.sendSms(from, to, text, (err, responseData) => {
                if (err) {
                  console.log(err);
                } else {
                  if (responseData.messages[0]["status"] === "0") {
                    console.log("Message sent successfully.");
                  } else {
                    console.log(
                      `Message failed with error: ${responseData.messages[0]["error-text"]}`
                    );
                  }
                }
              });

              //plese comment here

              ClientAppointmentModel.update(
                update,

                {
                  where: {
                    id: id,
                  },
                }
              )

                .then(async (data) => {
                  resolve(data);
                })
                .catch((err) => {
                  console.log(chalk.red("err: ey", err));
                  reject(err);
                });
            });
          }

          // if(!data){
          //   console.log("contact number 1", data.contact_no)
          // }else{
          //   console.log("contact number 2:", data.contact_no)

          // }
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  updateAppointmentAdmin: async (formData, id) => {
    return new Promise((resolve, reject) => {
      const formdata = {
        appointment_date: formData.appointment_date,
        appointment_time: formData.appointment_time,
        appointment_category: formData.appointment_category,
      };

      AppointmentModel.update(formdata, {
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //not modify reject o cancel
  rejectAppointmentClient: async (id, contactno) => {
    return new Promise(async(resolve, reject) => {

      const formdata = {
        user_status: "Rejected",
      };

     await ClientAppointmentModel.findOne({ where: { id: id } })
     .then(async(data) => {
          console.log(chalk.green("hello:", JSON.stringify(data)));

              ClientAppointmentModel.update(formdata, {
                  where: {
                    id: id,
                  },
                })
                .then((data) => {
                  console.log(chalk.green("data:", JSON.stringify(data)));

                  const from = "Dental Clinic";
                  const to = "63" + contactno;

                  // const text = "Your appointment has been cancelled, Please! rescheduled your appointment for dental clinic";

                    const text = "Unvailable";

                  vonage.message.sendSms(from, to, text, (err, responseData) => {
                    if (err) {
                      console.log(err);
                    } else {
                      if (responseData.messages[0]["status"] === "0") {
                        console.log("Message sent successfully.");
                      } else {
                        console.log(
                          `Message failed with error: ${responseData.messages[0]["error-text"]}`
                        );
                      }
                    }
                  });
    

                  resolve(data);
                })
                .catch((err) => {
                  console.log(chalk.red("err: ey", err));
                  reject(err);
                });


         

          // resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });

    //  console.log(chalk.green("hello:", JSON.stringify(project)));

      // ClientAppointmentModel.update(formdata, {
      //   where: {
      //     id: id,
      //   },
      // })
      //   .then((data) => {
      //     console.log(chalk.green("data:", JSON.stringify(data)));
      //     resolve(data);
      //   })
      //   .catch((err) => {
      //     console.log(chalk.red("err: ey", err));
      //     reject(err);
      //   });
    });
  },

  //APPOINTMENT HISTORY
  updateUserAppointment: async (formData, id) => {
    return new Promise((resolve, reject) => {
      const formdata = {
        client_id: formData.client_id,
        appointment_date: formData.appointment_date,
        appointment_time: formData.appointment_time,
        appointment_category: formData.appointment_category,
      };

      console.log("FROM SERVICE", formdata);

      ClientAppointmentModel.update(formdata, {
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  getSingleaAppointmentUser: async (id) => {
    return new Promise((resolve, reject) => {
      console.log("from services", id);

      ClientAppointmentModel.findOne({
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  fetchAllClientAppointmentCalendar: async () => {
    return new Promise((resolve, reject) => {
      ClientAppointmentModel.findAll()
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  GetAllReportUsersList: async (date) => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          "SELECT B.id, B.name, A.appointment_date, A.appointment_date, A.appointment_time, A.appointment_category, A.user_status FROM client_appointment_table A LEFT JOIN client_table B on B.id = A.client_id  WHERE A.appointment_date = :date group by B.id",
          {
            replacements: { date: date },
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //not finished 
  GetAllReportMonthlyList: async (date) => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          "SELECT (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Teeth Whitening' AND user_status = 'Scheduled') as count_teeth_whitening, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Root Canal' AND user_status = 'Scheduled') as count_root_canal, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Braces' AND user_status = 'Scheduled') as count_braces, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Fillings and Repair' AND user_status = 'Scheduled') as count_fil_repair, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Tooth extraction' AND user_status = 'Scheduled') as count_tooth_extract, (SELECT count(*) as count_all FROM client_appointment_table where user_status = 'Scheduled' ) as count_month, B.id, B.name, A.appointment_date, A.appointment_date, A.appointment_time, A.appointment_category, A.user_status FROM client_appointment_table A LEFT JOIN client_table B on B.id = A.client_id  WHERE month(A.created_date) = :date AND A.user_status = 'Scheduled' ",
          {
            replacements: { date: date },
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //not finished
  GetAllReportYearlyList: async (date) => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          "SELECT (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Teeth Whitening' AND user_status = 'Scheduled') as count_teeth_whitening, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Root Canal' AND user_status = 'Scheduled') as count_root_canal, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Braces' AND user_status = 'Scheduled') as count_braces, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Fillings and Repair' AND user_status = 'Scheduled') as count_fil_repair, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Tooth extraction' AND user_status = 'Scheduled') as count_tooth_extract, B.id, B.name, A.appointment_date, A.appointment_date, A.appointment_time, A.appointment_category, A.user_status FROM client_appointment_table A LEFT JOIN client_table B on B.id = A.client_id  WHERE year(A.created_date) = :date AND A.user_status = 'Scheduled' ",
          {
            replacements: { date: date },
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  //getall
  GetAllCountReportYear: async (date) => {
    return new Promise(async (resolve, reject) => {
      await db.sequelize
        .query(
          "SELECT (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Teeth Whitening' AND user_status = 'Scheduled') as count_teeth_whitening, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Root Canal' AND user_status = 'Scheduled') as count_root_canal, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Braces' AND user_status = 'Scheduled') as count_braces, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Fillings and Repair' AND user_status = 'Scheduled') as count_fil_repair, (SELECT count(*) as count_all FROM client_appointment_table where appointment_category = 'Tooth extraction' AND user_status = 'Scheduled') as count_tooth_extract  FROM client_appointment_table A WHERE year(A.created_date) = :date AND A.user_status = 'Scheduled' GROUP BY A.user_status ",
          {
            replacements: { date: date },
            plain: false,
            raw: false,
            type: QueryTypes.SELECT,
          }
        )
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },


};
