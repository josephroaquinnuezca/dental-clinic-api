const { QueryTypes } = require("sequelize");
// declaring models
const db = require("../models");

//initializing models
const ClientModel = db.client_table;

const Op = db.Sequelize.Op;
const chalk = require("chalk");
const async = require("async");


const Vonage = require("@vonage/server-sdk");

const vonage = new Vonage({
  apiKey: "6e396529",
  apiSecret: "wS4fCtmoHc4mNbM3"
})


const bcrypt = require("bcrypt");

module.exports = {
  // LOGIN
  login: async (username, password) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.yellow("TEAM USER Info:"));

      //var password_crypt = bcrypt.genSaltSync(10)
      // var uname = username
      // var pass = data.password
      // var passwordHash = bcrypt.compare(data.password, password_crypt)

      console.log("galing sa database", username);

      const condition = username ? { where: { username: username } } : null;

      ClientModel.findOne(condition)
        .then(async (data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          if (data) {
            const ValidatePassword = await bcrypt.compare(
              password,
              data.password
            );
            if (ValidatePassword) {
              resolve(data);
            } else {
              reject("Password does not match the record we found.");
            }
          } else {
            reject(
              "No result found for login data with username of " + username
            );
          }
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },
  //   CREATE USER END FUNCTIONS

  //CREATE USER FUNCTIONS
  getAllClient: async (limit, offset, searchString) => {
    return new Promise(async(resolve, reject) => {
      console.log(chalk.red("Function: getAllClient"));

      const condition = searchString
      ? {
          [Op.or]: [
            {
              name: {
                [Op.like]: `%${searchString}%`,
              },
            },
          ],

          [Op.and]: [
            {
              usr_role: 0,
            },
          ],

          // [Op.and]: [
          //   {
          //     status: id,
          //   },
          // ],
        }
      : null;
      

      await ClientModel.findAndCountAll(
       {
        limit: limit,
        offset: offset,
        where: condition,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          // console.log(chalk.green('data:', data));
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  getSingleClient: async (id) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.red("Function: GET ALL CLIENT DATA"));

      ClientModel.findByPk(id)

        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          // console.log(chalk.green('data:', data));
          resolve(data);
        })
        .catch((err) => {
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  //CREATE USER FUNCTIONS
  CreateALLClient: async (req_body) => {
    return new Promise(async (resolve, reject) => {
      // const t1 = await db.sequelize.transaction();

      var salt = await bcrypt.genSalt(10);
      var password_modify = await bcrypt.hash(req_body.password, salt);

      //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
      var data_message = {
        name: req_body.name,
        dob: req_body.dob,
        gender: req_body.gender,
        username: req_body.username,
        password: password_modify,
        contact_no: req_body.contact_no,
        age: req_body.age,
        email: req_body.email,
        address: req_body.address,
        date_created: req_body.date_created,
      };

      // const from = "Dental Clinic"
      // const to = "639952987343"
      // const text = 'Successfully Registered'

      // vonage.message.sendSms(from, to, text, (err, responseData) => {
      //     if (err) {
      //         console.log(err);
      //     } else {
      //         if(responseData.messages[0]['status'] === "0") {
      //             console.log("Message sent successfully.");
      //         } else {
      //             console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`);
      //         }
      //     }
      // })

      console.log(chalk.yellow("USERS INFO:", data_message));

      ClientModel.create(data_message, {
        omitNull: false,
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });

      // await t1.commit();
    });
  },
  //   CREATE USER END FUNCTIONS

  // UPDATE FUNCTIONS

  UpdateALLClient: async (req_body, id) => {
    return new Promise(async (resolve, reject) => {
      // const t1 = await db.sequelize.transaction();

      const password = req_body.password;

      if (password == "") {

        // var salt = await bcrypt.genSalt(10);
        // var password_modify = await bcrypt.hash(req_body.password, salt);

        //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
        var data_message = {
          name: req_body.name,
          dob: req_body.dob,
          gender: req_body.gender,
          username: req_body.username,
          contact_no: req_body.contact_no,
          age: req_body.age,
          email: req_body.email,
          address: req_body.address,
        };

        console.log(chalk.yellow("USERS INFO:", data_message));

        ClientModel.update(data_message, {
          where: {
            id: id,
          },
        })
          .then((data) => {
            console.log(chalk.green("data:", JSON.stringify(data)));

            resolve(data);
          })
          .catch((err) => {
            // await t1.rollback()
            console.log(chalk.red("err:", err));
            reject(err);
          });

      } else {


        var salt = await bcrypt.genSalt(10);
        var password_modify = await bcrypt.hash(req_body.password, salt);

        //SAVING INTO THE DATABASE -- CUSTOM VARIABLE THAT HOLDS DATA
        var data_message = {
          name: req_body.name,
          dob: req_body.dob,
          gender: req_body.gender,
          username: req_body.username,
          password: password_modify,
          contact_no: req_body.contact_no,
          age: req_body.age,
          email: req_body.email,
          address: req_body.address,
        };

        console.log(chalk.yellow("USERS INFO:", data_message));

        ClientModel.update(data_message, {
          where: {
            id: id,
          },
        })
          .then((data) => {
            console.log(chalk.green("data:", JSON.stringify(data)));

            resolve(data);
          })
          .catch((err) => {
            // await t1.rollback()
            console.log(chalk.red("err:", err));
            reject(err);
          });
      }
    });
  },

  GetCountAll: async () => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
      var query =
        "SELECT (SELECT COUNT(*) FROM client_table  WHERE user_status = 'Waiting') AS count_client_waiting, (SELECT COUNT(*) FROM client_table  WHERE user_status = 'Rejected') AS count_client_rejected, (SELECT COUNT(*) FROM client_table  WHERE user_status = 'Confirmed') AS count_client_confirmed, (SELECT  COUNT(*) FROM appointment_table WHERE appointment_date = '8/26/2021' ) as count_appointment_day, (SELECT  COUNT(distinct client_id) FROM client_appointment_table) as count_client_appointment , (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Fillings and Repair') AS count_fillings_repair, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Tooth extraction') AS count_tooth_extraction, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Braces') AS count_braces, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Root canal') AS count_root_canal, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Teeth Whitening') AS count_teeth_whitening, (SELECT COUNT(*) FROM client_appointment_table  WHERE user_status = 'Scheduled') AS count_patient_scheduled, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'January') AS January,(SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'February') AS February, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'March') AS March, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'April') AS April, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'May') AS May, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'June') AS June, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'July') AS July, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'August') AS August, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'September') AS September, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'October') AS October, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'November') AS November, (SELECT COUNT(*) FROM client_appointment_table  WHERE month = 'December') AS December";

      db.sequelize
        .query(query, {
          logging: console.log,
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
        })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  ConfirmedClient: async (id) => {
    return new Promise(async (resolve, reject) => {
      console.log(chalk.yellow("USER ID:", id));

      await ClientModel.findOne({
        where: {
          id: id,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          ClientModel.update(
            {
              user_status: "Confirmed",
            },
            {
              where: {
                id: id,
              },
            }
          )
            .then((data) => {
              resolve(data);
            })
            .catch((err) => {
              console.log(chalk.red("err: ey", err));
              reject(err);
            });
        })
        .catch((err) => {
          console.log(chalk.red("err: ey", err));
          reject(err);
        });
    });
  },

  GetCountWithDate: async (date) => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
      var query =
        "SELECT (SELECT  COUNT(*) FROM client_appointment_table WHERE user_status = 'Rejected' ) as count_rejected, (SELECT  COUNT(*) FROM client_appointment_table WHERE user_status = 'Waiting' ) as count_waiting,(SELECT  COUNT(*) FROM client_appointment_table WHERE user_status = 'Scheduled' ) as count_scheduled, (SELECT  COUNT(*) FROM appointment_table WHERE appointment_date = :date ) as count_appointment_day, (SELECT  COUNT(distinct client_id) FROM client_appointment_table) as count_client_appointment , (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Fillings and Repair'  && appointment_date = :date) AS count_fillings_repair, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Tooth extraction'  && appointment_date = :date) AS count_tooth_extraction, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Braces'  && appointment_date = :date) AS count_braces, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Root canal'  && appointment_date = :date) AS count_root_canal, (SELECT COUNT(*) FROM client_appointment_table  WHERE appointment_category = 'Teeth Whitening' && appointment_date = :date) AS count_teeth_whitening ";

      await db.sequelize
        .query(query, {
          logging: console.log,
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
          replacements: { date: date },
        })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  getalllUsersBydate: async (date) => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
    
      ClientModel.findAll({
        where: {
          date_created: date,
          user_role: 0,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  DeleteClient: async (id) => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
    
      ClientModel.destroy({
        where: {
          id: id
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));

          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  RejectClient: async (id) => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
      
      const updatedData = {
        user_status: 'Rejected',
      }

     await ClientModel.update(updatedData,{
        where: {
          id: id
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  //weekly report
  gellAllperWeekUser: async (startdate, enddate) => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
      
     console.log(startdate, enddate)

    ClientModel.findAll({
        where: {
          date_created: startdate,
          user_role: 0,
        },
      })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  gellAllperMonthUser: async (date) => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
      
     

      var query =
        "SELECT name, dob, gender, contact_no,  email, user_status, date_created, ceated_date FROM client_table where month(ceated_date) = :date";

      await db.sequelize
        .query(query, {
          logging: console.log,
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
          replacements: { date: date },
        })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },

  gellAllperYearUser: async (date) => {
    return new Promise(async (resolve, reject) => {
      // console.log(chalk.yellow("USERS INFO:", data_message));
      
     

      var query =
        "SELECT name, dob, gender, contact_no,  email, user_status, date_created, ceated_date FROM client_table where year(ceated_date) = :date";

      await db.sequelize
        .query(query, {
          logging: console.log,
          plain: false,
          raw: false,
          type: QueryTypes.SELECT,
          replacements: { date: date },
        })
        .then((data) => {
          console.log(chalk.green("data:", JSON.stringify(data)));
          resolve(data);
        })
        .catch((err) => {
          // await t1.rollback()
          console.log(chalk.red("err:", err));
          reject(err);
        });
    });
  },


  // (SELECT COUNT(*) FROM client_table  WHERE user_status = 'Waiting') AS count_client_waiting, (SELECT COUNT(*) FROM client_table  WHERE user_status = 'Rejected') AS count_client_rejected, (SELECT COUNT(*) FROM client_table  WHERE user_status = 'Confirmed') AS count_client_confirmed,

  // END OF UPDATE FUNCTIONS
};
