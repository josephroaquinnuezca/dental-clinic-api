--not totally done -- joseph nuezca 01-08-2021
CREATE TABLE `client_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `dob` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `contact_no` int NOT NULL,
  `email` varchar(45) NOT NULL,
  `age` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `appointment_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `client_id` int NOT NULL,
  `appointment_date` varchar(45) NOT NULL,
  `appointment_time` varchar(45) NOT NULL,
  `appointment_category` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT '9',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `client_appointment_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `client_id` int NOT NULL,
  `appointment_date` date NOT NULL,
  `appointment_time` varchar(45) NOT NULL,
  `appointment_category` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci